using System;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;
using System.Linq;

namespace WebApi.Dbworker
{
    /// <summary>
    ///  Служебный класс для работы с контекстом и соединением к БД
    /// </summary>
    internal class ApplicationContext : DbContext
    {
        public DbSet<Customer> Customer { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=avito;Username=postgres;Password=postgres");
        }
    }

    /// <summary>
    ///  Класс для внешнего взаимодействия с БД
    /// </summary>
    public class DBworker
    {
        public Customer ReturnCustomer(long id)
        {
            using (ApplicationContext db = new())
            {
                return db.Customer.FirstOrDefault(x => x.Id == id);
            }
        }

        public bool AddCustomer(Customer customer)
        {
            using (ApplicationContext db = new())
            {
                if (db.Customer.Any(x => x.Id != customer.Id))
                {
                    db.Customer.Add(customer);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}