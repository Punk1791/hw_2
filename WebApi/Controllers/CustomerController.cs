using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Dbworker;
using Newtonsoft.Json;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        DBworker db = new();

        [HttpPost]
        public IActionResult AddCustomer([FromBody] string msg)
        {
            var customer = JsonConvert.DeserializeObject<Customer>(msg);
            if (db.AddCustomer(customer))
            {
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        [HttpPost]
        public IActionResult ReturnCustomer([FromBody] string id)
        {
            var customer = db.ReturnCustomer(Convert.ToInt64(id));
            if (customer == null)
            {
                return BadRequest();
            }
            else
            {
                return Ok(JsonConvert.SerializeObject(customer));
            }
        }
    }
}