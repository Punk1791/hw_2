﻿using System;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static void Main(string[] args)
        {
            WebClient WebClient = new("http://localhost:5041/");

            bool loop = true;

            while (loop)
            {
                Console.WriteLine("Введите цифру 1 для проверки id, 2 - для создания нового пользователя, любое значение - для выхода");

                var numb = Console.ReadLine();
                switch (numb)
                {
                    case "1":
                        Console.WriteLine("Введите id");
                        var choice = Console.ReadLine();
                        WebClient.SendRequest(choice,"Customer/ReturnCustomer");
                        break;
                    case "2":
                        Console.WriteLine("Введите id");
                        var id = Console.ReadLine();
                        var newCustomer = new CustomerCreateRequest().GenerateCustomer(Convert.ToInt64(id));
                        WebClient.SendRequest(newCustomer,"Customer/AddCustomer");
                        break;
                    default:
                        loop = false;
                        break;
                }
            }
        }
    }
}