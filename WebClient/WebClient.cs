using System;
using System.Net;
using System.Net.Http;

namespace WebClient
{
    public class WebClient
    {
        string baseUri;

        public WebClient(string uri)
        {
            baseUri = uri;
        }
        public async void SendRequest(string msg, string addr)
        {
            using(HttpClient client = new())
            {
                HttpContent request = new StringContent(msg);

                var result = await client.PostAsync(baseUri+addr, request);

                ReturnResultToConsole(result);
            }
        }

        private void ReturnResultToConsole(HttpResponseMessage result)
        {
            if(result.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine(result.Content.ReadAsStringAsync());
            }
            else
            {
                Console.WriteLine($"Error! Code:{(int)result.StatusCode}");
            }
        }
    }
}