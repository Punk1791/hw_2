using Newtonsoft.Json;
using System;
using System.Linq;

namespace WebClient
{
    public class CustomerCreateRequest
    {
        public CustomerCreateRequest()
        {
        }

        public CustomerCreateRequest(string firstName,string lastName)
        {
            Firstname = firstName;
            Lastname = lastName;
        }

        public string GenerateCustomer(long id)
        {
            var customer = new Customer()
            {
                Id = id,
                Firstname = RandomString(10),
                Lastname = RandomString(10)
            };
            return JsonConvert.SerializeObject(customer);
        }

        private static string RandomString(int length)
        {
            Random random = new ();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}